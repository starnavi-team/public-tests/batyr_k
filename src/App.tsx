import Tree from './components/tree'
import json from './data.json'

function App() {
  return <Tree data={json.data} />
}

export default App

export interface TreeNode {
  id: number
  label: string
  nodes: TreeNode[]
}
