import { TreeNode } from '../../App'
import Branch from '../branch'

interface Props {
  data: TreeNode[]
}

const Tree = ({ data }: Props) => {
  return (
    <>
      {data.map(item => (
        <Branch item={item} level={0} key={item.id} />
      ))}
    </>
  )
}

export default Tree
