import { FC } from 'react'
import { TreeNode } from '../../App'
import { Wrapper, Button, Label } from './styles'
import { FiChevronUp, FiChevronDown } from 'react-icons/fi'

interface Props {
  item: TreeNode
  isCollapsed: boolean
  level: number
  onToggle: () => void
  hasChildren: boolean
}

const Node: FC<Props> = props => {
  const { item, isCollapsed, level, onToggle, hasChildren, children } = props

  return (
    <Wrapper level={level}>
      <Label>
        {item.label}
        
        {hasChildren && (
          <Button onClick={onToggle}>
            {isCollapsed ? (
              <FiChevronUp size={16} />
            ) : (
              <FiChevronDown size={16} />
            )}
          </Button>
        )}
      </Label>
      {children}
    </Wrapper>
  )
}

export default Node
