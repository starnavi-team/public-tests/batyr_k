import styled from '@emotion/styled'

export const Wrapper = styled.div<{ level: number }>`
  position: relative;
  padding: 4px;
  padding-left: ${({ level }) => level * 8}px;
  ::before {
    content: '';
    position: absolute;
    height: 100%;
    top: 0;
    left: ${({ level }) => level * 4}px;
    width: 1px;
    border-left: ${({ level }) => level !== 0 && '1px solid #f0f0f0'};
  }
`

export const Label = styled.div`
  display: flex;
  align-items: center;
  column-gap: 4px;
`

export const Button = styled.button`
  outline: none;
  border: none;
  border-radius: 4px;
  padding: 2px;
  background-color: #ebebeb80;
  display: flex;
  align-items: center;
  justify-content: center;

  :hover {
    background-color: #ebebeb;
  }
`
