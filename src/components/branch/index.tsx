import React, { useState } from 'react'
import { TreeNode } from '../../App'
import Node from '../node'

interface Props {
  item: TreeNode
  level: number
}

const Branch = ({ item, level }: Props) => {
  const [isCollapsed, setIsCollapsed] = useState<boolean>(true)

  const toggle = () => {
    setIsCollapsed(prev => !prev)
  }
  const hasChildren = Boolean(item?.nodes?.length)

  const renderBranches = () => {
    if (!hasChildren) return null

    const newLevel = level + 1

    return item.nodes.map((child: any) => (
      <Branch key={child.id} level={newLevel} item={child} />
    ))
  }

  return (

      <Node
        item={item}
        isCollapsed={isCollapsed}
        level={level}
        onToggle={toggle}
        hasChildren={hasChildren}
      >
        {!isCollapsed && renderBranches()}
      </Node>
  )
}

export default Branch
